package com.example.rohitbit123.logindesign;

import android.app.Activity;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

//<!-- copyrighted content owned by Android Arena (www.androidarena.co.in)-->
//package com.example.logindesign;


public class MainActivity extends Activity {
    EditText name,email,mobile;
    Button log;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        name=(EditText)findViewById(R.id.name);
        email=(EditText)findViewById(R.id.email);
        mobile=(EditText)findViewById(R.id.mobile);
        log=(Button)findViewById(R.id.btn);

        log.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String nm=name.getText().toString();
                String em=email.getText().toString();
                String mob=mobile.getText().toString();
                new Logindata().getdata(nm,em,mob);



            }
        });



    }


}
